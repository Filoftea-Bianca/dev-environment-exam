#!/bin/sh

echo "** RTE mode: $RTE"

python manage.py check
python manage.py makemigrations
python manage.py migrate

case "$RTE" in
	dev )
		echo "** In the development mode"
		pip-audit
		coverage run --source="." --omit=manage.py manage.py test --verbosity 2
		coverage report -m
		python manage.py runserver 0.0.0.0:8000
		;;
	test )
		echo "** In the testing mode"
		pip-audit || exit 1
		coverage run --source="." --omit=manage.py manage.py test --verbosity 2
		coverage report -m --fail-under=80
		;;
	prod )
		echo "** In the production mode"
		pip-audit || exit 1
		python manage.py check --deploy
		;;
esac
	
