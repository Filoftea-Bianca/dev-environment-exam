DROP TABLE IF EXISTS ourtodo;

CREATE TABLE ourtodo (
	id serial primary id,
	ts timestamp default Now(),
	tx text
)
