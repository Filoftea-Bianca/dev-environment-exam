asgiref==3.5.2
Django==4.1
pytz==2021.3
sqlparse==0.4.2
psycopg2-binary
coverage
pip-audit==2.4.3

